var parents = {
    'Henry': {'childName': 'Calvin', 'age': 1},
    'Ada': {'childName': 'Lily', 'age': 4},
    'Emilia': {'childName': 'Petra', 'age': 2},
    'Biff': {'childName': 'Biff Jr', 'age': 3},
    'Milo': {}
}

var activities = [
    {
        'age': 1,
        'activity': [
            'Go outside and feel surfaces.',
            'Try singing a song together.',
            'Point and name objects.'
            ]
    },
    {
        'age': 2,
        'activity': [
            'Draw with crayons.',
            'Play with soundmaking toys or instruments.',
            'Look at family pictures together.'
            ]
    },
    {
        'age': 3,
        'activity': [
            'Build with blocks.',
            'Try a simple puzzle.',
            'Read a story together.'
            ]
    }
]

for (i in parents) {

    if (!Object.keys(parents[i]).length) break;
    var ctr = 0;
    for (x = 0; x < activities.length; x++) {
        if (parents[i]['age'] == activities[x]['age']) {
            console.info('Parent:' + i);
            console.info('Child: ' + parents[i]['childName']);
            console.info(activities[x]['activity'].join('\n'));
            ctr++;
        }
    }

    if (ctr) console.info('Curriculum complete! \n');
}

